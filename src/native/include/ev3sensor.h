/*
 * EV3 Sensor API
 *
 * Copyright (C) 2014 Carsten Zeiffert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/
 *
 * ----------------------------------------------------------------------------
 *
 * \author Simón Rodriguez Perez(Hochschule Aschaffenburg)
 * \date 2015-02-28
 * \version 2
 * \note Correct data for Touch-, Sonar- and Lightsensor

 * ----------------------------------------------------------------------------
 *
 * \author Faculty of Electrical Engineering, CTU in Prague (Jakub Vanek <nxc4ev3@vankovi.net>)
 * \date 2017-01-0
 * \version 3
 * \note Add getSensorMode function.
 * 
 * ----------------------------------------------------------------------------
 *
 * \author Faculty of Electrical Engineering, CTU in Prague (Jakub Vanek <nxc4ev3@vankovi.net>)
 * \date 2017-09-05
 * \version 4
 * \note Fix sensor types.
 *
 */
#ifndef EV3SENSOR_H
#define EV3SENSOR_H

#include "ev3sensor_dispatch.h"

/**
 * Initializes sensor I/O.
 */
int initSensors();

/**
 * Reads the sensor value from a specific port.
 * Example: readSensor(INPUT_1)
 * Returns a raw sensor value.
 */
int readSensor(int sensorPort);

/**
 * Reads the sensor value from a specific port with a specific slot.
 * Example: readSensor(INPUT_1, 0)
 * Returns a raw sensor value.
 */
int readSensorSlot(int sensorPort, int slot);

/**
 * Returns pointer to the current sensor value.
 * The sensor value may be up to 32 bytes long - this function
 * can be used to access it if readSensor() is inadequate.
 */
bufptr_t readSensorData(int sensorPort);

/**
 * Get configured sensor mode for a specific port.
 * Example: setSensorMode(INPUT_1)
 */
int getSensorMode(int sensorPort);

/**
 * Set sensor mode for a specific port.
 * Example: setSensorMode(INPUT_1, COL_REFLECT)
 */
int setSensorMode(int sensorPort, int name);

/**
 * Set sensor mode for a all ports
 * Note: Can be only called once
 * Example: setAllSensorMode(TOUCH_PRESS, US_DIST_MM, NO_SEN, COL_COLOR)
 */
int setAllSensorMode(int name_1, int name_2, int name_3, int name_4);


/**
 * Get configured sensor ev3type for a specific port.
 */
int getSensorType(int sensorPort);

/**
 * Set sensor ev3type for a specific port.
 */
int setSensorType(int sensorPort, int type);

/***********************************/

enum SensorNames {
    // no sensor
    NO_SEN,

    // touch sensor
    TOUCH_PRESS,

    // color sensor
    COL_REFLECT,
    COL_REFLECT_RAW,
    COL_AMBIENT,
    COL_COLOR_ID,
    COL_COLOR_RAW,

    // ultrasonic sensor
    US_DIST_EMUL,
    US_DIST_MM,
    US_DIST_IN,
    US_SI_MM,
    US_SI_IN,
    US_LISTEN,

    // gyro sensor
    GYRO_ANGLE,
    GYRO_RATE,
    GYRO_BOTH,
    GYRO_FAS,

    // infra sensor
    IR_PROX,
    IR_SEEK,
    IR_REMOTE,

    // nxt sensors
    NXT_IR_SEEKER,
    NXT_TEMP_C,
    NXT_TEMP_F,
};

enum InfraMessages {
    BEACON_OFF,
    BEACON_UP_LEFT,
    BEACON_DOWN_LEFT,
    BEACON_UP_RIGHT,
    BEACON_DOWN_RIGHT,
    BEACON_UP,
    BEACON_DIAG_UP_LEFT,
    BEACON_DIAG_UP_RIGHT,
    BEACON_DOWN,
    BEACON_ON,
    BEACON_LEFT,
    BEACON_RIGHT,
};


enum EV3SensorType {
	EV3_Color,
	EV3_Touch,
	EV3_Sonic,
	EV3_Gyro,
	EV3_Infra
};

enum EV3SonicMode {
	Sonic_Continuous_Mm,
	Sonic_Continuous_In,
	Sonic_Single_Mm,
	Sonic_Single_In,
	Sonic_Listen,
};

enum EV3GyroMode {
	Gyro_Angle,
	Gyro_Rate,
	Gyro_AngleRate,
	Gyro_Fas,
};

enum EV3ColorMode {
	Color_Reflect,
	Color_ReflectRaw,
	Color_Ambient,
	Color_ColorId,
	Color_ColorRaw,
};

enum EV3TouchMode {
	Touch_Touch,
};

enum EV3InfraMode {
	Infra_Proximity,
	Infra_Seek,
	Infra_Remote,
};


#endif // EV3SENSOR_H
