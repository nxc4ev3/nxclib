/*
 * EV3 Sensor API
 *
 * Copyright (C) 2014 Carsten Zeiffert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/
 *
 * ----------------------------------------------------------------------------
 *
 * \author Simón Rodriguez Perez(Hochschule Aschaffenburg)
 * \date 2015-02-28
 * \version 2
 * \note Correct readout for Touch-, Sonar- and Lightsensor
 *
 * ----------------------------------------------------------------------------
 *
 * \author Simón Rodriguez Perez
 * \date 2016-04-20
 * \version 3
 * \note Correct readout for Gyroscop and Infrared Sensor
 *
 */
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include "ev3sensor.h"
#include "ev3typedata.h"
#include "ev3sensor_dispatch.h"
#include "ev3_command.h"


#define   ANALOG_DEVICE_NAME    "/dev/lms_analog"
#define     UART_DEVICE_NAME    "/dev/lms_uart"
#define      IIC_DEVICE_NAME    "/dev/lms_iic"
#define      DCM_DEVICE_NAME    "/dev/lms_dcm"

typedef struct {
	volatile UART   * pUart;
	volatile IIC    * pIic;
	volatile ANALOG * pAnalog;

	int fdUart;
	int fdIic;
	int fdAnalog;
	int fdDcm;
} ev3sensor_shm_t;

typedef struct {
	DEVCON devs;
	DATA8  inputmodes  [INPUTS];
	int    names       [INPUTS];
	int    ev3type     [INPUTS];
} ev3sensor_state_t;

ev3sensor_shm_t     mem;
ev3sensor_state_t state;



int mmap_file  (char *name, size_t len, int *fd, void **ptr);
int munmap_file(int fd, void *ptr, size_t len);
int uart_upload();
int uart_off();
int uart_wait_off();
int uart_on();
int uart_wait_ready();
int uart_identify();
int uart_zero_state();


/********************************************************************************************/
/**
* Initialisation of the Sensorfunctions
* modified by: Simón Rodriguez Perez
* 		 date: 2015-02-28
* 
* modified by: Faculty of Electrical Engineering, CTU in Prague (Jakub Vanek <nxc4ev3@vankovi.net>)
* 		 date: 2017-01-07
* 		 note: sensor setup array is now initialized with no sensor setups
*
*/
int initSensors() {
#ifdef DEBUG_SENSORS_REDIR
	freopen("/tmp/log.txt", "a+", stdout);
	dup2(fileno(stdout), fileno(stderr));
#endif
	
	int Result = 0;
	int i;

	Result |= open_file(   DCM_DEVICE_NAME,                 &mem.fdDcm);
	Result |= mmap_file(ANALOG_DEVICE_NAME, sizeof(UART),   &mem.fdAnalog, (void**)&mem.pAnalog);
	Result |= mmap_file(  UART_DEVICE_NAME, sizeof(IIC),    &mem.fdUart,   (void**)&mem.pUart  );
	Result |= mmap_file(   IIC_DEVICE_NAME, sizeof(ANALOG), &mem.fdIic,    (void**)&mem.pIic   );

	if (Result != 0) {
		munmap_file(mem.fdAnalog, (void*)mem.pAnalog, sizeof(ANALOG));
		munmap_file(mem.fdIic,    (void*)mem.pIic,    sizeof(IIC)   );
		munmap_file(mem.fdUart,   (void*)mem.pUart,   sizeof(UART)  );
		return 1;
	}

	// initialize basic internal state
	for (i = 0; i < INPUTS; i++) {
		state.names     [i] = NO_SEN;
		state.ev3type   [i] = EV3_Touch;
		state.inputmodes[i] = '-';
	}

#ifdef DEBUG_SENSORS
	puts("UART manipulation start.");
#endif
	//Result |= uart_off();
	//puts("UART shutdown sent.");
	//Result |= uart_wait_off();
	//puts("UART off.");

	Result |= uart_on();
#ifdef DEBUG_SENSORS
	puts("UART on.");
#endif

	Result |= uart_wait_ready();
#ifdef DEBUG_SENSORS
	puts("UART ready.");
#endif
	
	Result |= uart_identify();
#ifdef DEBUG_SENSORS
	puts("UART identified.");
#endif

	return !!Result;
}

int open_file(char *name, int *pFd) {
	int fd;
	
	fd = open(name, O_RDWR | O_SYNC);
	if (fd == 0) {
		fprintf(stderr, "Cannot open %s: %s (code %d)\n", name, strerror(errno), errno);
		return 1;
	}
	
	*pFd  = fd;
	return 0;
}

int mmap_file(char *name, size_t len, int *pFd, void **pPtr) {
	int fd;
	void *ptr;
	
	if (open_file(name, &fd) != 0) {
		return 1;
	}
	
	ptr = mmap(0, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	if (ptr == 0) {
		fprintf(stderr, "Cannot mmap %s: %s (code %d)\n", name, strerror(errno), errno);
		return 1;
	}
	
	*pFd  = fd;
	*pPtr = ptr;
	return 0;
}

int munmap_file(int fd, void *ptr, size_t len) {
	int Result = 0;
	if (ptr == NULL)
		return 0;
	
	if (munmap(ptr, len) != 0) {
		fprintf(stderr, "Cannot munmap file: %s (code %d)\n", strerror(errno), errno);
		Result = 1;
	}
	if (close(fd) != 0) {
		fprintf(stderr, "Cannot munmap file: %s (code %d)\n", strerror(errno), errno);
		Result = 1;
	}
	return Result;
}

int uart_upload() {
	uart_zero_state();
	if (ioctl(mem.fdUart, UART_SET_CONN, &state.devs) != 0) {
		fprintf(stderr, "Cannot invoke UART_SET_CONN: %s (code %d)\n", strerror(errno), errno);
		return 1;
	}
	if (write(mem.fdDcm, state.inputmodes, sizeof(state.inputmodes)) != 0) {
		fprintf(stderr, "Cannot set iopins: %s (code %d)\n", strerror(errno), errno);
		return 1;
	}
	return 0;
}

int uart_off() {
	int i;
	for (i = 0; i < INPUTS; i++) {
		state.devs.Connection[i] = CONN_NONE;
		state.devs.Type      [i] = 0;
		state.devs.Mode      [i] = 0;
		state.inputmodes     [i] = '-';
	}
	return uart_upload();
}

int uart_wait_off() {
	uart_zero_state();
	// noop
	return 0;
}

int uart_on() {
	int i;
	for (i = 0; i < INPUTS; i++) {
		state.devs.Connection[i] = mem.pAnalog->InConn[i];
		state.devs.Type      [i] = mem.pAnalog->InDcm [i];
		state.devs.Mode      [i] = 0;
		state.inputmodes     [i] = '-';
	}
	return uart_upload();
}

int uart_wait_ready() {
	int i;
	for (i = 0; i < INPUTS; i++) {
		if (state.devs.Connection[i] != CONN_INPUT_UART) {
			continue;
		}
		while ((mem.pUart->Status[i] & UART_DATA_READY) == 0) {
			mem.pUart->Status[i] &= ~(UART_PORT_CHANGED | UART_WRITE_REQUEST);
		}
	}
	return 0;
}

int uart_identify() {
	int i;
	for (i = 0; i < INPUTS; i++) {
		if (state.devs.Connection[i] != CONN_INPUT_UART) {
			continue;
		}
		state.names[i] = stp_get_name(&mem.pUart->TypeData[i][0]);
	}
	return 0;
}
int uart_zero_state() {
	int i;
	for (i = 0; i < INPUTS; i++) {
		mem.pUart->Status[i] = 0;
	}
	return 0;
}

/*DATA8 getSensorConnectionType(int sensorPort)
{
	if (!g_analogSensors || sensorPort < 0 || sensorPort >= INPUTS)
		return 0;
	return g_analogSensors->InConn[sensorPort];
}*/

/********************************************************************************************/
/**
* Getting the Data from a Uartport
* modified by: Simón Rodriguez Perez
* 		 date: 2015-02-28
* 		 note: Readout of Uart-Port
*
*/
bufptr_t readUartSensor(int sensorPort)
{
	UWORD slot = mem.pUart->Actual[sensorPort];
	return mem.pUart->Raw[sensorPort][slot];
}

bufptr_t readIicSensor(int sensorPort)
{
    UWORD slot = mem.pIic->Actual[sensorPort];
    return mem.pIic->Raw[sensorPort][slot];
}

bufptr_t readNewDumbSensor(int sensorPort)
{
	return (void*)&(mem.pAnalog->InPin6[sensorPort]);
}

bufptr_t readOldDumbSensor(int sensorPort)
{
	return (void*)&(mem.pAnalog->InPin1[sensorPort]);
}

bufptr_t readNull(int unused) {
	return NULL;
}

/********************************************************************************************/
/**
* Get the Data from the Sensor
* modified by: Simón Rodriguez Perez
* 		 date: 2015-02-28
* 		 note: changed for Touch-, Sonar- and Lightsensor
*
*----------------------------------------------------------------------------------
*
* modified by: Simón Rodriguez Perez
* 		 date: 2016-04-21
* 		 note: readout for Gyro and Infrared Sensor
*
*/
bufptr_t readSensorData(int sensorPort)
{
	if (sensorPort < 0 || sensorPort >= INPUTS)
		return 0;

	sensor_read_fn f = stp_get_reader(state.names[sensorPort]);
	if (f == NULL) {
		f = readNull;
	}
	
	return f(sensorPort);
}

/********************************************************************************************/
/**
* Usercall for actual value of one Sensor
* modified by: Simón Rodriguez Perez
* 		 date: 2015-02-28
* 		 note: now working for Touch-, Sonar- and Lightsensor
*			   with calculation of the correct values
*
*----------------------------------------------------------------------------------
*
* modified by: Simón Rodriguez Perez
* 		 date: 2016-04-21
* 		 note: readout for Gyroscop and Infrared Sensor
* 
*----------------------------------------------------------------------------------
*
* modified by: Jakub Vaněk
* 		 date: 2018-05-01
* 		 note: added support for slots
*
*/
int readSensor(int sensorPort)
{
#ifdef DEBUG_SENSORS
	printf("status = '%02hhX', tdata = '%02hhX'\r", mem.pUart->Status, (int)mem.pUart->TypeData[sensorPort][0].Name[0]);
#endif
	bufptr_t data = readSensorData(sensorPort);
	if (!data)
		return -1;
	
	sensor_parse_fn f = stp_get_parser(state.names[sensorPort]);
	if (f == NULL) {
		f = parseDefault;
	}
	
	return f(sensorPort, 0, data);
}

/********************************************************************************************/
/**
* Usercall for actual value of one Sensor
* modified by: Simón Rodriguez Perez
* 		 date: 2015-02-28
* 		 note: now working for Touch-, Sonar- and Lightsensor
*			   with calculation of the correct values
*
*----------------------------------------------------------------------------------
*
* modified by: Simón Rodriguez Perez
* 		 date: 2016-04-21
* 		 note: readout for Gyroscop and Infrared Sensor
* 
*----------------------------------------------------------------------------------
*
* modified by: Jakub Vaněk
* 		 date: 2018-05-01
* 		 note: added support for slots
*
*/
int readSensorSlot(int sensorPort, int slot)
{
#ifdef DEBUG_SENSORS
	printf("status = '%02hhX', tdata = '%02hhX'\r", mem.pUart->Status, (int)mem.pUart->TypeData[sensorPort][0].Name[0]);
#endif
	bufptr_t data = readSensorData(sensorPort);
	if (!data)
		return -1;
	
	sensor_parse_fn f = stp_get_parser(state.names[sensorPort]);
	if (f == NULL) {
		f = parseDefault;
	}
	
	return f(sensorPort, slot, data);
}



/********************************************************************************************/
/**
* Reading configured sensor mode 
* modified by: Faculty of Electrical Engineering, CTU in Prague (Jakub Vanek <nxc4ev3@vankovi.net>)
* 		 date: 2017-01-07
*
*/


int getSensorMode(int sensorPort) {
	if (sensorPort < 0 || sensorPort >= INPUTS)
		return -1;
	return state.names[sensorPort];
}


/********************************************************************************************/
/**
* Initialisation for one Sensor 
* modified by: Simón Rodriguez Perez
* 		 date: 2015-02-28
* 		 note: Sensors are working now, but only one sensor is working at once
* modified by: Jakub Vaněk
*        date: 2017-09-05
*        note: Typedata split
*
*/
int setSensorMode(int sensorPort, int name)
{
	if (sensorPort < 0 || sensorPort >= INPUTS)
		return -1;
	
	state.names[sensorPort] = name;
	stp_fill_kernel(name, sensorPort, &state.devs, &state.inputmodes);
	
	if (!uart_upload()) {
		return -1;
	}
	return uart_wait_ready();
}

/********************************************************************************************/
/**
* Initialisation of all Sensors for working parallel 
* author: Simón Rodriguez Perez
* note: the function can only be called once at the beginning
* modified by: Jakub Vaněk
*        date: 2017-09-05
*        note: Typedata split
*
*/
int setAllSensorMode(int name_1, int name_2, int name_3, int name_4)
{
	state.names[0] = name_1;
	state.names[1] = name_2;
	state.names[2] = name_3;
	state.names[3] = name_4;
	stp_fill_kernel_all(state.names, &state.devs, &state.inputmodes);

	if (!uart_upload()) {
		return -1;
	}
	return uart_wait_ready();
}


/********************************************************************************************/
/**
 * Get configured sensor ev3type for a specific port.
 */
int getSensorType(int sensorPort) {
	return state.ev3type[sensorPort];
}

/********************************************************************************************/
/**
 * Set sensor ev3type for a specific port.
 */
int setSensorType(int sensorPort, int type) {
	state.ev3type[sensorPort] = type;
}
