/*
 * Parse received sensors data
 *
 * Copyright (C) 2017 Faculty of Electrical Engineering, CTU in Prague (Jakub Vanek <nxc4ev3@vankovi.net>)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/
 *
 * ----------------------------------------------------------------------------
 *
 * \author Faculty of Electrical Engineering, CTU in Prague (Jakub Vanek <nxc4ev3@vankovi.net>)
 * \date 2017-09-06
 * \version 1
 *
 */


#include <stdint.h>
#include "ev3sensor_dispatch.h"
#include "ev3sensor.h"
#include "basictypes.h"
#include "typedata.h"


#define AS_S8(var)   ((  int8_t*)var)
#define AS_S16(var)  (( int16_t*)var)
#define AS_U16(var)  ((uint16_t*)var)
#define AS_S32(var)  (( int32_t*)var)
#define AS_F(var)    ((   float*)var)

int parseNull(int port, int slot, bufptr_t data) {
	return -1;
}

int parseTouch(int port, int slot, bufptr_t data) {
	SWORD real = *AS_S16(data) / 256;
	
	if (real == 0)
		return 0;
	else if(real == 0xD)
		return 1;
	else
		return -1;
}

int parseEv3Uart8    (int port, int slot, bufptr_t data) {
	return *(AS_S8(data) + slot);
}

int parseEv3Uart16   (int port, int slot, bufptr_t data) {
	return *(AS_S16(data) + slot);
}

int parseEv3Uart32   (int port, int slot, bufptr_t data) {
	return *(AS_S32(data) + slot);
}

int parseEv3UartF    (int port, int slot, bufptr_t data) {
	return *(AS_F(data) + slot);
}

int parseDistEmul    (int port, int slot, bufptr_t data) {
	return parseEv3Uart16(port, 0, data) / 10;
}

// NXT
int parseNXTIR_Seeker(int port, int slot, bufptr_t data) {
	return *AS_S8(data) & 0x0F;
}

int parseNXTTemp_C(int port, int slot, bufptr_t data) {
	uint16_t raw = *AS_U16(data);
	raw = raw >> 4; // cut unneeded zeros
	
	int8_t mult = 1;
	if ((raw & 0x800) != 0) { // negative number, perform two's complement inversion
		raw = (raw & 0x7FF) ^ 0x7FF + 1;
		mult = -1;
	}
	
	int16_t units   = (raw >> 4) * 10;
	int16_t decimal = (raw & 0xF) * 10 / 15; // 15 probably for rounding
	
	return mult * (units + decimal);
}

int parseNXTTemp_F(int port, int slot, bufptr_t data) {
	return parseNXTTemp_C(port, slot, data) * 9 / 5 + 320;
}

int parseDefault(int port, int slot, bufptr_t data) {
	return *(AS_S16(data) + slot);
}

