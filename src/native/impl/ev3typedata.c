/*
 * EV3 internal sensor typedata API
 *
 * Copyright (C) 2017 Faculty of Electrical Engineering, CTU in Prague (Jakub Vanek <nxc4ev3@vankovi.net>)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/
 *
 * ----------------------------------------------------------------------------
 *
 * \author Faculty of Electrical Engineering, CTU in Prague (Jakub Vanek <nxc4ev3@vankovi.net>)
 * \date 2017-09-05
 * \version 1
 *
 */
#include <stdlib.h>

#include "ev3sensor.h"
#include "ev3sensor_dispatch.h"
#include "ev3typedata.h"

// THIS ARRAY MUST STAY SORTED! #BinarySearch
const sensor_type_table typedata = {
	// no sensor
	{ NO_SEN,          CONN_NONE,                  0, 0               ,  0x2D, readNull,          parseNull         },
	// touch sensor
	{ TOUCH_PRESS,     CONN_INPUT_DUMB,   TOUCH_TYPE, TOUCH_PRESS_MODE,  0x46, readNewDumbSensor, parseTouch        },
	// color sensor
	{ COL_REFLECT,     CONN_INPUT_UART,     COL_TYPE, COL_REFLECT_MODE,  0x2D, readUartSensor,    parseEv3Uart8     },
	{ COL_REFLECT_RAW, CONN_INPUT_UART,     COL_TYPE, COL_REFRAW_MODE,   0x2D, readUartSensor,    parseEv3Uart16    },
	{ COL_AMBIENT,     CONN_INPUT_UART,     COL_TYPE, COL_AMBIENT_MODE,  0x2D, readUartSensor,    parseEv3Uart8     },
	{ COL_COLOR_ID,    CONN_INPUT_UART,     COL_TYPE, COL_COLORID_MODE,  0x2D, readUartSensor,    parseEv3Uart8     },
	{ COL_COLOR_RAW,   CONN_INPUT_UART,     COL_TYPE, COL_COLRAW_MODE,   0x2D, readUartSensor,    parseEv3Uart16    },
	// ultrasonic sensor
	{ US_DIST_EMUL,    CONN_INPUT_UART,      US_TYPE, US_DIST_EMUL_MODE, 0x2D, readUartSensor,    parseDistEmul     },
	{ US_DIST_MM,      CONN_INPUT_UART,      US_TYPE, US_DIST_MM_MODE,   0x2D, readUartSensor,    parseEv3Uart16    },
	{ US_DIST_IN,      CONN_INPUT_UART,      US_TYPE, US_DIST_IN_MODE,   0x2D, readUartSensor,    parseEv3Uart16    },
	{ US_SI_MM,        CONN_INPUT_UART,      US_TYPE, US_SI_MM_MODE,     0x2D, readUartSensor,    parseEv3Uart16    },
	{ US_SI_IN,        CONN_INPUT_UART,      US_TYPE, US_SI_IN_MODE,     0x2D, readUartSensor,    parseEv3Uart16    },
	{ US_LISTEN,       CONN_INPUT_UART,      US_TYPE, US_LISTEN_MODE,    0x2D, readUartSensor,    parseEv3Uart8     },
	// gyroscopic sensor
	{ GYRO_ANGLE,      CONN_INPUT_UART,    GYRO_TYPE, GYRO_ANG_MODE,     0x2D, readUartSensor,    parseEv3Uart16    },
	{ GYRO_RATE,       CONN_INPUT_UART,    GYRO_TYPE, GYRO_RATE_MODE,    0x2D, readUartSensor,    parseEv3Uart16    },
	{ GYRO_BOTH,       CONN_INPUT_UART,    GYRO_TYPE, GYRO_COMBO_MODE,   0x2D, readUartSensor,    parseEv3Uart16    },
	{ GYRO_FAS,        CONN_INPUT_UART,    GYRO_TYPE, GYRO_FAS_MODE,     0x2D, readUartSensor,    parseEv3Uart16    },
	// ir sensor
	{ IR_PROX,         CONN_INPUT_UART,      IR_TYPE, IR_PROX_MODE,      0x2D, readUartSensor,    parseEv3Uart8    },
	{ IR_SEEK,         CONN_INPUT_UART,      IR_TYPE, IR_SEEK_MODE,      0x2D, readUartSensor,    parseEv3Uart8    },
	{ IR_REMOTE,       CONN_INPUT_UART,      IR_TYPE, IR_REMOTE_MODE,    0x2D, readUartSensor,    parseEv3Uart8    },
    
	// old nxt sensors, no idea whether they actually work, probably not
	{ NXT_IR_SEEKER,   CONN_NXT_IIC,        IIC_TYPE, IIC_BYTE_MODE,     0x46, readIicSensor,     parseNXTIR_Seeker },
	{ NXT_TEMP_C,      CONN_NXT_IIC,   NXT_TEMP_TYPE, NXT_TEMP_C_MODE,   0x46, readIicSensor,     parseNXTTemp_C    },
	{ NXT_TEMP_F,      CONN_NXT_IIC,   NXT_TEMP_TYPE, NXT_TEMP_F_MODE,   0x46, readIicSensor,     parseNXTTemp_F    },
};

const size_t typedata_length = sizeof(typedata) / sizeof(typedata[0]);

// cast back to struct, access 'name' member and compare
static int stp_compare_name(const void *anon_a, const void *anon_b) {
	const sensor_typedata *a = (const sensor_typedata *)anon_a;
	const sensor_typedata *b = (const sensor_typedata *)anon_b;
	return a->name - b->name; // works, everything should be in range
}


// binary search for struct with same 'name' member
sensor_typedata *stp_find(int name) {
	sensor_typedata this_sensor = {name, 0, 0, 0};
	void *result = bsearch(&this_sensor,
	                       typedata,
	                       typedata_length,
	                       sizeof(sensor_typedata),
	                       &stp_compare_name);
	return (sensor_typedata *) result;
}

// copy data from typedata to DEVCON
int stp_fill_kernel(int name, int port, DEVCON *output, DATA8 *iopin) {
	sensor_typedata *data = stp_find(name);
	
	if (data == NULL)
		return -1;
	
	output->Connection[port] = data->connection;
	output->Type      [port] = data->type;
	output->Mode      [port] = data->mode;
	iopin             [port] = data->input;
	
	return 0;
}

// iterate on the ports; call stp_fill_kernel on all of them
int stp_fill_kernel_all(int names[INPUTS], DEVCON *output, DATA8 *iopin) {
	int result;
	for (int port = 0; port < INPUTS; port++) {
		result = stp_fill_kernel(names[port], port, output, iopin);
		
		if (result != 0)
			return -1;
	}
	return 0;
}

// find entry & get 'reader' member
sensor_read_fn stp_get_reader(int name) {
	sensor_typedata *this = stp_find(name);
	if (this == NULL)
		return NULL;
	return this->reader;
}

// find entry & get 'parse' member
sensor_parse_fn stp_get_parser(int name) {
	sensor_typedata *this = stp_find(name);
	if (this == NULL)
		return NULL;
	return this->parser;
}

int stp_get_name(volatile TYPES *remote) {
	const sensor_typedata *ptr;
	const sensor_typedata *begin = typedata;
	const sensor_typedata *end = typedata + typedata_length;
	for (ptr = begin; ptr != end; ptr++) {
		if (ptr->connection == remote->Connection &&
		    ptr->type       == remote->Type &&
		    ptr->mode       == remote->Mode) {
			return ptr->name;
		}
	}
	return -1;
}

int stp_mode_number(int name) {
	const sensor_typedata *data = stp_find(name);
	if (!data)
		return -1;
	return data->mode;
}

DATA8 stp_get_inputmode(int name) {
	const sensor_typedata *data = stp_find(name);
	if (!data)
		return -1;
	return data->input;
}
